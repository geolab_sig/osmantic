package org.openstreetmap.josm.plugins.osmantic;


import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.sqlite.SQLiteConfig;

public class SqliteConnection {
	Statement  stat;
	Connection conn;
	/**
	 * @param args
	 */
	public SqliteConnection()  {
		try {	
			String dbPath = new File(System.getProperty("java.io.tmpdir"),"osmantic.db").getPath();
			File osmDbFile = new File(dbPath);
			
			this.conn = obtainSqliteDbConnection(osmDbFile);
			if(conn != null){
				this.stat = conn.createStatement();
			}			
		} catch (Exception e){
			e.printStackTrace();		
		}
	}	
	
	private static Connection obtainSqliteDbConnection(File dbFile) {
		Connection connection = null;
		try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
        	e.printStackTrace();
        }
        
        try {
            SQLiteConfig config = new SQLiteConfig();
            config.setReadOnly(true);
            //connection = DriverManager.getConnection("jdbc:sqlite:" + dbFile.getAbsolutePath(), config.toProperties());
            connection = DriverManager.getConnection("jdbc:sqlite:" + dbFile, config.toProperties());
            return connection;
        } catch (SQLException e) {
        	e.printStackTrace();
        }
        
        return connection;
    }	
}