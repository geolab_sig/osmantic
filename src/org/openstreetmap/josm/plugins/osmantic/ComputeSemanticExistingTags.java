package org.openstreetmap.josm.plugins.osmantic;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.openstreetmap.josm.data.osm.OsmPrimitive;
import org.openstreetmap.josm.data.osm.OsmPrimitiveType;


public class ComputeSemanticExistingTags{
	OSManticDialog OSMANTICDIALOG;	
	SqliteConnection sqlite;
	DecimalFormat df = new DecimalFormat("#.00");
	
	public ComputeSemanticExistingTags(OsmPrimitive osmprim, OSManticDialog OSManticDialog){
		sqlite = new SqliteConnection() ;
		OSMANTICDIALOG = OSManticDialog;
		OSMANTICDIALOG.resetArrayResults();		
		if(!osmprim.getKeys().isEmpty()){			
			querySemanticDB(osmprim.getType(), osmprim.getKeys());
		}		
	}	
	protected void querySemanticDB(OsmPrimitiveType geomType, final Map<String, String> UNK){	
		final OsmPrimitiveType GEOM = geomType;	
		Map<String, Float> osmComputation = new HashMap<String, Float>();
		for(Entry<String, String> tag : UNK.entrySet()){						
			Map<String, Float> sqliteResults = sqlQuery(GEOM.name(), tag.getKey(), tag.getValue());
			if(sqliteResults.size()>0){						
				for( Entry<String, Float> sqliteResult : sqliteResults.entrySet() ){							
					osmComputation.put(sqliteResult.getKey(), sqliteResult.getValue());					
				}
			}					
		}			
		
		float totalN = 0;
		for( Entry<String, Float> probability : osmComputation.entrySet() ){
			totalN += probability.getValue();
		}				
		
		for( Entry<String, Float> osmC : osmComputation.entrySet() ){
			String[] kv = osmC.getKey().split("=");	
			if(!UNK.containsKey(kv[0])){			
				NumberFormat nf = NumberFormat.getInstance();				 
				Double perValue = 0.00;				
				try {
					perValue = nf.parse( df.format((osmC.getValue()*100)/totalN) ).doubleValue();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				
				OSMANTICDIALOG.updateSmanticResults( 
						osmC.getKey(), 
						perValue,
						"<html>"+ kv[0]+"="+kv[1]+  "  (<a href='http://wiki.openstreetmap.org/wiki/Tag:"+kv[0]+"%3D"+kv[1]+"'>wiki</a>)</html>"
				);
			}else{
				String v = null;
				for(Entry<String, String> unk : UNK.entrySet()){
					if(unk.getKey().matches(kv[0])){
						v = unk.getValue();
					}
				}
				if(!v.contains(kv[1])){
					NumberFormat nf = NumberFormat.getInstance();				 
					Double perValue = 0.00;				
					try {
						perValue = nf.parse( df.format((osmC.getValue()*100)/totalN) ).doubleValue();
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}	
					OSMANTICDIALOG.updateSmanticResults( 
							osmC.getKey(), 
							perValue,
							"<html>"+ kv[0]+"="+kv[1]+  "  (<a href='http://wiki.openstreetmap.org/wiki/Tag:"+kv[0]+"%3D"+kv[1]+"'>wiki</a>)</html>"
					);
				}
			}
		}
		OSMANTICDIALOG.tableModel.fireTableDataChanged();				
	}
	
	public Map<String, Float> sqlQuery(String geomType, String key, String value){		
		Map<String, Float> hashMap = new HashMap<String, Float>();
		if(sqlite.stat != null){			
			try {				
				String sql = "SELECT key1, value1, key2, value2, count_all, semantic_value" +
							 " FROM osmantic WHERE " +
							 " (key1 LIKE ? OR key2 LIKE ?)"+
							 " AND (value1 LIKE ? OR value2 LIKE ?)"+					
							 " ORDER BY count_all DESC LIMIT 10;";			
				PreparedStatement pStat = sqlite.conn.prepareStatement(sql);
				pStat.setString(1, key); pStat.setString(2, key);
				pStat.setString(3, value); pStat.setString(4, value);
				ResultSet rs = pStat.executeQuery();
							
				while (rs.next()) {					
					String rowKey, rowValue;					
					if(rs.getString("key1").matches(key) && rs.getString("value1").matches(value)){
						rowKey 	 = rs.getString("key2");
						rowValue = rs.getString("value2");					
					}else{
						rowKey 	 = rs.getString("key1");
						rowValue = rs.getString("value1");
					}
					String tag = rowKey+"="+rowValue;		
										
					float probability;					
					if(rs.getString("count_all") != null){
						if(rs.getString("semantic_value") != null){
							Float count_all = Float.parseFloat(rs.getString("count_all"));
							Float semantic_value = Float.parseFloat(rs.getString("semantic_value"))+1;
							probability = count_all*semantic_value;
						}else{
							probability = Float.parseFloat(rs.getString("count_all"));							
						}						
					}else{
						probability = (float) 0.00;
					}			
					hashMap.put(tag, probability);					
				}
	        } catch (SQLException e) {
	        	e.printStackTrace();
	        }
		}			
		return hashMap;
	}
}
