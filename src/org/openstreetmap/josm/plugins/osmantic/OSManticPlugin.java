package org.openstreetmap.josm.plugins.osmantic;



import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map.Entry;
import java.util.Set;

import org.openstreetmap.josm.Main;
import org.openstreetmap.josm.data.SelectionChangedListener;
import org.openstreetmap.josm.data.osm.DataSet;
import org.openstreetmap.josm.data.osm.OsmPrimitive;
import org.openstreetmap.josm.data.osm.event.AbstractDatasetChangedEvent;
import org.openstreetmap.josm.data.osm.event.DataChangedEvent;
import org.openstreetmap.josm.data.osm.event.DataSetListener;
import org.openstreetmap.josm.data.osm.event.NodeMovedEvent;
import org.openstreetmap.josm.data.osm.event.PrimitivesAddedEvent;
import org.openstreetmap.josm.data.osm.event.PrimitivesRemovedEvent;
import org.openstreetmap.josm.data.osm.event.RelationMembersChangedEvent;
import org.openstreetmap.josm.data.osm.event.TagsChangedEvent;
import org.openstreetmap.josm.data.osm.event.WayNodesChangedEvent;
import org.openstreetmap.josm.gui.MapFrame;
import org.openstreetmap.josm.gui.MapView;
import org.openstreetmap.josm.gui.MapView.LayerChangeListener;
import org.openstreetmap.josm.gui.layer.Layer;
import org.openstreetmap.josm.gui.layer.OsmDataLayer;
import org.openstreetmap.josm.plugins.Plugin;
import org.openstreetmap.josm.plugins.PluginInformation;


public class OSManticPlugin extends Plugin implements SelectionChangedListener, DataSetListener, LayerChangeListener {
	int flag = 0;
	protected static OSManticDialog OSManticDialog;
	private OsmPrimitive PREVIOUSSELECTION = null;
	String[] AvoidTags = new String[] {"source","note","ref","attribution","official_status","name"};

	public OSManticPlugin(PluginInformation info) {
		super(info);
		copyOSManticDB();
	}

	private void copyOSManticDB() {
		InputStream osmDumpPath = getClass().getResourceAsStream("/resources/osmantic.db");
		File osmDbTmpPath = new File(System.getProperty("java.io.tmpdir"),"osmantic.db");		
		try {
			if (osmDbTmpPath.exists()==false){
				osmDbTmpPath.createNewFile();
				OutputStream out1 = new FileOutputStream(osmDbTmpPath);
		        byte[] buf = new byte[1024];
		        int len;
		        while ((len = osmDumpPath.read(buf)) > 0) {
		            out1.write(buf, 0, len);
		        }
		        out1.flush();
		        out1.close();
		        osmDumpPath.close();				
			}			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}				
	}

	@Override
	public void mapFrameInitialized(MapFrame oldFrame, MapFrame newFrame) {
		if (oldFrame == null && newFrame != null) {
			// Construct the Dialog component
			OSManticDialog = new OSManticDialog();
			newFrame.addToggleDialog(OSManticDialog);
			MapView.addLayerChangeListener(this);
		}
	}

	@Override
	public void layerAdded(Layer newLayer) {
		if (newLayer instanceof OsmDataLayer) {
			OsmDataLayer osmdatalayer = (OsmDataLayer) newLayer;
			osmdatalayer.data.addDataSetListener(this);
			DataSet.addSelectionListener(this);
		}
	}

	@Override
	public void activeLayerChange(Layer oldLayer, Layer newLayer) {

	}

	@Override
	public void layerRemoved(Layer oldLayer) {

	}

	@Override
	public void primitivesAdded(PrimitivesAddedEvent event) {
		
	}

	@Override
	public void primitivesRemoved(PrimitivesRemovedEvent event) {

	}

	@Override
	public void tagsChanged(TagsChangedEvent event) {	
		OsmPrimitive osmprim = event.getPrimitive();
		if(event.getPrimitive().getKeys().entrySet().size()>0){					
			Entry<String, String> newTags = null;
			Set<Entry<String, String>> filter = event.getOriginalKeys().entrySet();
			for( Entry<String, String> entry : event.getPrimitive().getKeys().entrySet() ) {
			    if( !filter.contains( entry ) ){
			    	newTags = entry;
			    }
			}			
			if(newTags != null){
				if(!Arrays.asList(AvoidTags).contains(newTags.getKey())){	
					if(filter.size()>0){
						for( Entry<String, String> entry : event.getPrimitive().getKeys().entrySet() ) {
						    if( !filter.contains( entry ) ){
						    	newTags = entry;
						    }
						}					
						new CheckSemanticRelation(newTags, event.getOriginalKeys().entrySet(), OSManticDialog);						
					}			
				}
			}			
		}
		new ComputeSemanticExistingTags(osmprim, OSManticDialog);
	}

	@Override
	public void nodeMoved(NodeMovedEvent event) {


	}

	@Override
	public void wayNodesChanged(WayNodesChangedEvent event) {


	}

	@Override
	public void relationMembersChanged(RelationMembersChangedEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void otherDatasetChange(AbstractDatasetChangedEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dataChanged(DataChangedEvent event) {
		// TODO Auto-generated method stub

	}

	@Override
	public void selectionChanged(Collection<? extends OsmPrimitive> newSelection) {
		this.flag += 1;
		//the pugin works only for one selected object			
		Collection<OsmPrimitive> RealSel = Main.main.getInProgressSelection();
		if( RealSel.size()==1 ){
			for(OsmPrimitive osmprim : RealSel){				
				if( !osmprim.equals(PREVIOUSSELECTION) && osmprim.getKeys().size()!=0 ){				
					new ComputeSemanticExistingTags(osmprim, OSManticDialog);
				}
				PREVIOUSSELECTION = osmprim;
			}
		}else{
			OSManticDialog.resetArrayResults();
		}
	}
}
