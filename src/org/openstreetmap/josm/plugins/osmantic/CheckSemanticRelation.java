package org.openstreetmap.josm.plugins.osmantic;

import static org.openstreetmap.josm.tools.I18n.tr;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.JOptionPane;

import org.openstreetmap.josm.gui.Notification;

public class CheckSemanticRelation {	
	SqliteConnection sqlite;
	String[] AvoidTags = new String[] {"source","note","ref","attribution","official_status", "name"};
	public CheckSemanticRelation(Entry<String, String> newTags, Set<Entry<String, String>> tagsDataset,OSManticDialog OSManticDialog) {
		sqlite = new SqliteConnection();		
		for(Iterator<Entry<String, String>> itCurrentDataset = tagsDataset.iterator(); itCurrentDataset.hasNext(); ){
			Entry<String, String> e = itCurrentDataset.next();			
			if(!Arrays.asList(AvoidTags).contains(e.getKey())){
				if(!sqlCheckSimilarity(newTags, e)){
					String msg = tr("Warning, low relationship detected between the newly added tag ("+newTags.getKey()+"="+newTags.getValue()+") <br />"
							+ " & "+ e.getKey()+"="+ e.getValue() +". Please ensure there is a relationship.");
					new Notification(msg).setIcon(JOptionPane.WARNING_MESSAGE).show();
				};
			}
		}	
	}
	
	public boolean sqlCheckSimilarity(Entry<String, String> tag1, Entry<String, String> tag2){
		String sql = "SELECT count(*) as c" +
				 " FROM osmantic WHERE " +
				 " key1 LIKE ? AND value1 LIKE ? AND key2 LIKE ? and value2 LIKE ?"+				 
				 " LIMIT 1;";

		String sqlInverse = "SELECT count(*) as c" +
				 " FROM osmantic WHERE " +
				 " key2 LIKE ? AND value2 LIKE ? AND key1 LIKE ? and value1 LIKE ?"+				 
				 " LIMIT 1;";
		
		boolean isRelation = false;
		try {
			PreparedStatement pStat = sqlite.conn.prepareStatement(sql);
			pStat.setString(1, tag1.getKey()); pStat.setString(2, tag1.getValue());
			pStat.setString(3, tag2.getKey()); pStat.setString(4, tag2.getValue());		
			ResultSet rs = pStat.executeQuery();			
			while(rs.next()){
				if(rs.getBoolean("c")){
					isRelation = true;
				}
			}
			PreparedStatement pStatInverse = sqlite.conn.prepareStatement(sqlInverse);
			pStatInverse.setString(1, tag1.getKey()); pStatInverse.setString(2, tag1.getValue());
			pStatInverse.setString(3, tag2.getKey()); pStatInverse.setString(4, tag2.getValue());		
			ResultSet rsInverse = pStatInverse.executeQuery();			
			while(rsInverse.next()){
				if(rsInverse.getBoolean("c")){
					isRelation = true;
				}
			}
		} catch (SQLException e) {
	        	e.printStackTrace();
        }
		return isRelation;
	}
}