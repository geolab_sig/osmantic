package org.openstreetmap.josm.plugins.osmantic;


import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;


public class OSMSemanticPresentationPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static JCheckBox chckbxNewCheckBox = null;

	/**
	 * Create the panel.
	 */


	public OSMSemanticPresentationPanel() {
		setMinimumSize(new Dimension(580, 330));
		setMaximumSize(new Dimension(580, 330));
		setPreferredSize(new Dimension(580, 340));
		setBounds(new Rectangle(5, 5, 5, 5));

		JLabel lblWelcomeToThe = new JLabel("<html>"
				+	"<span style=\"color: rgb(204, 85, 0); font-size: large;\"> Welcome </span> <span style=\"font-size: large;\">to the OSM</span> <br />"
				+	"<span style=\"font-size: xx-large;\">Semantic Plugin</span>"
				+ 	"</html>"
				);

		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setAlignmentY(Component.TOP_ALIGNMENT);

		lblNewLabel_2.setText("<html>"
				+ "<span style=\"color: rgb(204, 85, 0);\">OSM Semantic Plugin</span> is a research tool developed by the "
				+ "<span style=\"color: rgb(204, 85, 0);\">Geomatics Research Lab</span> of <br />"
				+ "Memorial University of Newfoundland <br />"
				+ "<br />"
				+ "The aim of this plugin is to link <span style=\"color: rgb(204, 85, 0);\">VGI</span> Data <br /> "
				+ "with <span style=\"color: rgb(204, 85, 0);\">Semantic</span> Data.<br />"
				+ "<br />"
				+ "Semantic similarity measurements are computed<br />"
				+ " to improve the overall quality of the VGI dataset and to enhance users' experience"
				+ "</html>"
				);
		lblNewLabel_2.setSize(new Dimension(260, 0));

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(getClass().getResource("/images/dialogs/rdf_world_big.png")));

		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(getClass().getResource("/images/icones/MUN_Logo_RGB.png")));

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
										.addComponent(lblWelcomeToThe, GroupLayout.PREFERRED_SIZE, 312, GroupLayout.PREFERRED_SIZE)
										.addGap(126)
										.addComponent(lblNewLabel))
										.addGroup(groupLayout.createSequentialGroup()
												.addComponent(lblNewLabel_1, GroupLayout.PREFERRED_SIZE, 214, GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(lblNewLabel_2, GroupLayout.PREFERRED_SIZE, 333, GroupLayout.PREFERRED_SIZE)))
												.addGap(43))
				);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
										.addContainerGap()
										.addComponent(lblNewLabel))
										.addGroup(groupLayout.createSequentialGroup()
												.addGap(6)
												.addComponent(lblWelcomeToThe)))
												.addPreferredGap(ComponentPlacement.RELATED)
												.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addGroup(groupLayout.createSequentialGroup()
																.addGap(24)
																.addComponent(lblNewLabel_2, GroupLayout.PREFERRED_SIZE, 194, GroupLayout.PREFERRED_SIZE))
																.addGroup(groupLayout.createSequentialGroup()
																		.addGap(72)
																		.addComponent(lblNewLabel_1)))
																		.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				);
		setLayout(groupLayout);

	}

	public boolean isChkBoxSelected() {
		return chckbxNewCheckBox.isSelected();
	}
}
