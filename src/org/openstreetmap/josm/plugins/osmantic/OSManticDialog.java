package org.openstreetmap.josm.plugins.osmantic;

import static org.openstreetmap.josm.tools.I18n.tr;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import org.openstreetmap.josm.Main;
import org.openstreetmap.josm.command.ChangePropertyCommand;
import org.openstreetmap.josm.data.osm.OsmPrimitive;
import org.openstreetmap.josm.gui.ConditionalOptionPaneUtil;
import org.openstreetmap.josm.gui.dialogs.ToggleDialog;
import org.openstreetmap.josm.tools.OpenBrowser;

public class OSManticDialog  extends ToggleDialog {

	private static final long serialVersionUID = 1L;
	private static Boolean firstShow = true;
	private final OSMSemanticPresentationPanel OSMSemanticPresentationPanel = new OSMSemanticPresentationPanel();
	private Pattern pattern = Pattern.compile("<a href='(.*?)'>");
	Collection<OsmPrimitive> sel;
	JTable table;
	protected DefaultTableModel tableModel;
	
	public OSManticDialog(){
		super( tr("OSM Semantic"), "rdf_world.png", tr("Add semantic capacities."), null, 150 );

			
		String questionnaireStr = "<HTML>Please, help us improve this plugin "
									+ "by filling this <FONT color=\"#000099\"><U>short questionnaire</U></FONT>.</HTML>";
		Border paddingBorder = BorderFactory.createEmptyBorder(10,10,10,10);
		JLabel questionnaire = new JLabel();
		questionnaire.setBorder(paddingBorder);
		questionnaire.setText(questionnaireStr);
		questionnaire.setCursor(new Cursor(Cursor.HAND_CURSOR));		

		try {
			final URI uri = new URI("https://sites.google.com/site/osmanticjosm/");		
			questionnaire.setToolTipText(uri.toString());			
			questionnaire.addMouseListener(new MouseListener(){
				@Override
				public void mouseClicked(MouseEvent e) {
					OpenBrowser.displayUrl(uri);				
				}
	
				@Override
				public void mousePressed(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
	
				@Override
				public void mouseReleased(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
	
				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
	
				@Override
				public void mouseExited(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}			
			});
		} catch (URISyntaxException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		JPanel valuePanel = new JPanel(new BorderLayout());
		String[] colHeadings = {"Tag Name","Probability %", "Wiki"};
		tableModel = new DefaultTableModel(1, colHeadings.length) {
			private static final long serialVersionUID = 1L;
			@Override			
			public boolean isCellEditable(int row, int column) {
				return false;
			}
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
			      switch (columnIndex) {
			      case 0:
			         return String.class;
			      case 1:
			         return Double.class;
			      case 2:
			         return String.class;			      
			      default:
			         return Object.class;
			      }
			   }
		};	
		
		
		tableModel.setColumnIdentifiers(colHeadings);
		
	    table = new JTable(tableModel);			
		table.setAutoCreateRowSorter(true);	
		table.setRowSorter(null);
		table.getColumnModel().getColumn(1).setMaxWidth(110);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(tableModel);
		table.setRowSorter(sorter);
		List<RowSorter.SortKey> sortKeys = new ArrayList<RowSorter.SortKey>();
		sortKeys.add(new RowSorter.SortKey(tableModel.findColumn("Probability %"), SortOrder.DESCENDING));
		sorter.setSortKeys(sortKeys);
		
		
		tableModel.setRowCount(0);
		JScrollPane scrollPane = new JScrollPane(table);
		valuePanel.add(questionnaire, BorderLayout.NORTH);
		valuePanel.add(scrollPane, BorderLayout.CENTER);
		createLayout(valuePanel, false, null);	
		
		table.addMouseListener(new MouseListener(){
			@Override
			public void mouseClicked(MouseEvent e) {
				JTable target = (JTable) e.getSource();
				int row = target.getSelectedRow();
				int col = target.getSelectedColumn();
				
				//Wiki click
				if(col==2){
					Object value = target.getValueAt(row, col);
					Matcher m = pattern.matcher(value.toString());					
					if (m.find()) {
						URI uri;
						try {
							uri = new URI(m.group(1));
							OpenBrowser.displayUrl(uri);
						} catch (URISyntaxException e1) {						
							e1.printStackTrace();
						} 					    
					}					
				}
				
				if (e.getClickCount() == 2) {			
					try{
						if(target.getRowCount()>0){						
							//tag click
							if(col==0){
								Object value = target.getValueAt(row, col);		
								String[] tag = value.toString().split("=");
								sel = Main.main.getInProgressSelection();
								if (sel == null || sel.isEmpty() || sel.size()>1) return;
								Main.main.undoRedo.add(new ChangePropertyCommand(sel, tag[0], tag[1]));					
							}
						}
					}catch (Exception exep){
						System.out.println(exep);
					}	
				}
			}
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}			
		});
	}

	@Override
	public void showDialog() {
		//display plugin splash screen
		this.button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AbstractButton abstractButton = (AbstractButton) e.getSource();
				ButtonModel buttonModel = abstractButton.getModel();
				if(buttonModel.isSelected() && firstShow == true){
					ConditionalOptionPaneUtil.showMessageDialog(
							"OSMantic-splashscreen",
							Main.parent,
							OSMSemanticPresentationPanel,
							tr("OSMantic Info"),
							JOptionPane.INFORMATION_MESSAGE
							);
					firstShow = false;
				}
			}
		});
		super.showDialog();
	}

	public void resetArrayResults(){
		if(tableModel.getRowCount()>0){
			tableModel.setRowCount(0);
		}
	}

	public void updateSmanticResults(String tag, double i, String hyperlink){
		tableModel.addRow(new Object[]{
				tag, i, hyperlink
		});				
		return;				
	}
}